﻿using CommandLine;
using System.ComponentModel.DataAnnotations;

namespace IPConnectivityChecker.Util;

public static class CommandLineOptions
{
    public class BucketManagerOptions
    {
        [Option("dummy", Required = false, Default = false, HelpText = "An argument that tells the program to execute in dummy mode.")]
        public bool DummyMode { get; set; }

        [Option("ecs", Required = true, HelpText = "The location of the Dell ECS system to test. Valid options are 'rwth', 'nrw', 'tudo', and 'ude'.")]
        [EnumDataType(typeof(Enums.EcsLocations))]
        public Enums.EcsLocations EcsLocation { get; set; }

        [Option("resource-types", Required = true, HelpText = "The location of the Dell ECS system to test. Valid options are 'rds' and 'rdss3'.", Separator = ',')]
        [EnumDataType(typeof(Enums.ResourceTypes))]
        public IEnumerable<Enums.ResourceTypes> ResourceTypes { get; set; } = null!;
    }
}