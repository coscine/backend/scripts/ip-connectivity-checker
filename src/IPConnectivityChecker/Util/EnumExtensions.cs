﻿using System.Runtime.Serialization;

namespace IPConnectivityChecker.Util;

public static class EnumExtensions
{
    public static string? GetEnumMemberValue<T>(this T member) where T : Enum
    {
        var enumType = typeof(T);
        var memberName = Enum.GetName(enumType, member);
        if (memberName is null)
            return null;

        var memberInfo = enumType.GetField(memberName);
        var attribute = memberInfo?.GetCustomAttributes(typeof(EnumMemberAttribute), false).FirstOrDefault() as EnumMemberAttribute;

        return attribute?.Value;
    }
}
