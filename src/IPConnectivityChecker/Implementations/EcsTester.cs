﻿using Amazon.S3;
using Coscine.ECSManager;
using Coscine.ResourceTypes;
using Coscine.ResourceTypes.Base;
using Coscine.ResourceTypes.Rds;
using Coscine.ResourceTypes.RdsS3;
using Coscine.ResourceTypes.RdsS3Worm;
using Coscine.ResourceTypes.ResourceTypeConfigs;
using IPConnectivityChecker.Util;
using NLog;
using static IPConnectivityChecker.Util.CommandLineOptions;

namespace IPConnectivityChecker.Implementations;

public class EcsTester
{
    public long Quota { get; init; }
    public Guid Guid { get; init; }
    public string BucketName { get; init; }
    public BucketManagerOptions Options { get; init; }

    private readonly ILogger _logger;
    private readonly string _testPrefix = "TEST-Coscine-";
    private const string format = " {0,-25} [{1}]\t{2}";
    private bool working = true;

    public EcsTester(BucketManagerOptions options, ILogger logger)
    {
        Quota = 1;
        Guid = Guid.NewGuid();
        BucketName = $"{_testPrefix}{Guid.ToString()[..8]}";
        Options = options;
        _logger = logger;
    }

    public async Task<bool> RunAsync()
    {
        var location = Options.EcsLocation.GetEnumMemberValue();
        foreach (var resourceTypeToTest in Options.ResourceTypes)
        {
            var resourceType = resourceTypeToTest.GetEnumMemberValue();
            var specificType = resourceType + location;

            if (resourceType is not null && specificType is not null)
            {
                Console.WriteLine($"\n{new string('=', 79)}\n Resource Type: {resourceType.ToUpper()} -- {location?.ToUpper()}\n{new string('-', 79)}");
                try
                {
                    BaseResourceType resourceTypeDefinition = ResourceTypeFactory.Instance.GetResourceType(resourceType, specificType);
                    _logger.Info("Working on {resourceTypeToTest}", resourceTypeToTest);
                    switch (resourceTypeToTest)
                    {
                        case Enums.ResourceTypes.RDS_Web:
                            var rdsWebDefinition = (RdsResourceType)resourceTypeDefinition;
                            var rdsWebConfiguration = rdsWebDefinition.RdsResourceTypeConfiguration;

                            await TestRdsWebClientAsync(rdsWebConfiguration);
                            await TestRdsWebEcsAuthAsync(rdsWebConfiguration);
                            await TestBucketsRdsWebAsync(specificType);
                            break;

                        case Enums.ResourceTypes.RDS_S3:
                            var rdsS3Definition = (RdsS3ResourceType)resourceTypeDefinition;
                            var rdsS3Configuration = rdsS3Definition.RdsS3ResourceTypeConfiguration;

                            await TestRdsS3ClientAsync(rdsS3Configuration);
                            await TestRdsS3EcsAuthAsync(rdsS3Configuration);
                            await TestBucketsRdsS3Async(specificType);
                            break;

                        case Enums.ResourceTypes.RDS_WORM:
                            var rdsWormDefinition = (RdsS3WormResourceType)resourceTypeDefinition;
                            var rdsWormConfiguration = rdsWormDefinition.RdsS3WormResourceTypeConfiguration;

                            await TestRdsWormClientAsync(rdsWormConfiguration);
                            await TestRdsWormEcsAuthAsync(rdsWormConfiguration);
                            // No bucket creation testing, due to the nature of this resource type
                            Console.WriteLine(string.Format(format, "Bucket Testing", "--", "Skipped due to the nature of this resource type "));
                            break;

                        default:
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine($"!! Error inside Resource Type Factory: {e.Message}");
                    continue;
                }
            }
            else
                Console.WriteLine($"\nWARNING!!!\n Resource type {resourceTypeToTest} could not be parsed!");

            
        }
        Console.WriteLine(new string('=', 79));
        return working;
    }
    private async Task TestRdsWebEcsAuthAsync(RdsResourceTypeConfiguration rdsWebConfiguration)
    {
        var ecsAuthMessage = await EcsAuthentication(rdsWebConfiguration.EcsManagerConfiguration);
        if (string.IsNullOrWhiteSpace(ecsAuthMessage))
        {
            Console.WriteLine(string.Format(format, "Auth Check", "OK", $"ECS login & logout for object user successful"));
            working = working && true;
        }
        else
        {
            Console.WriteLine(string.Format(format, "Auth Check", "FAILED", ecsAuthMessage));
            working = working && false;
        }
    }
    private async Task TestRdsWebClientAsync(RdsResourceTypeConfiguration rdsWebConfiguration)
    {
        var (ecsClientMessage, ecsBucketCount) = await S3ClientCheck(rdsWebConfiguration.Endpoint, rdsWebConfiguration.AccessKey, rdsWebConfiguration.SecretKey);
        if (string.IsNullOrWhiteSpace(ecsClientMessage))
        {
            Console.WriteLine(string.Format(format, "Web Client Check", "OK", $"Found {ecsBucketCount} entries"));
            working = working && true;
        }
        else
        {
            Console.WriteLine(string.Format(format, "Web Client Check", "FAILED", ecsClientMessage));
            working = working && false;
        }
    }
    public async Task TestBucketsRdsWebAsync(string specificType)
    {
        try
        {
            const string _type = "rds";
            var rdsEcsManager = CreateRdsWebManager(_type, specificType);

            var resourceType = ResourceTypeFactory.Instance.GetResourceType(_type, specificType);
            await resourceType.CreateResource(BucketName, Quota);
            _logger.Info("Bucket created {BucketName} with {Quota} GB quota", BucketName, Quota);
            Console.WriteLine(string.Format(format, "Bucket Creation", "OK", $"Bucket creation in namespace successful"));

            var randomFileName = $"{_testPrefix}DELETE-ME-{Guid.NewGuid()}.txt";

            var memoryStream = new MemoryStream();
            byte[] testData = { (byte)'C', (byte)'o', (byte)'S', (byte)'I', (byte)'n', (byte)'e' };

            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            await resourceType.StoreEntry(BucketName, randomFileName, memoryStream);
            _logger.Info("Stored file {randomFileName}", randomFileName);

            var entry = await resourceType.GetEntry(BucketName, randomFileName);
            var entries = await resourceType.ListEntries(BucketName, "");
            Console.WriteLine(string.Format(format, "File Storing", "OK", $"Stored files in bucket: {entries.Count}"));

            await resourceType.DeleteEntry(BucketName, entry!.Key);
            Console.WriteLine(string.Format(format, "File Deletion", "OK", $"Files in bucket deletion successful"));
            await rdsEcsManager.DeleteBucket(BucketName);
            _logger.Info("Bucket successfully deleted {BucketName}", BucketName);
            Console.WriteLine(string.Format(format, "Bucket Deletion", "OK", $"Deletion of bucket successful"));
            working = working && true;
        }
        catch (Exception e)
        {
            Console.WriteLine(string.Format(format, "Bucket Testing", "FAILED", e.Message));
            working = working && false;
            _logger.Error("RDS Web Testing error on {BucketName} with {Quota} GB quota", BucketName, Quota);
        }
    }


    private async Task TestRdsS3EcsAuthAsync(RdsS3ResourceTypeConfiguration rdsS3Configuration)
    {
        var ecsObjectUserAuthMessage = await EcsAuthentication(rdsS3Configuration.RdsS3EcsManagerConfiguration);
        if (string.IsNullOrWhiteSpace(ecsObjectUserAuthMessage))
        {
            Console.WriteLine(string.Format(format, "Object-User Auth Check", "OK", $"ECS login & logout for object user successful"));
            working = working && true;
        }
        else
        {
            Console.WriteLine(string.Format(format, "Object-User Auth Check", "FAILED", ecsObjectUserAuthMessage));
            working = working && false;
        }

        var ecsUserAuthMessage = await EcsAuthentication(rdsS3Configuration.UserEcsManagerConfiguration);
        if (string.IsNullOrWhiteSpace(ecsUserAuthMessage))
        {
            Console.WriteLine(string.Format(format, "User Auth Check", "OK", $"ECS login & logout for user successful"));
            working = working && true;
        }
        else
        {
            Console.WriteLine(string.Format(format, "User Auth Check", "FAILED", ecsUserAuthMessage));
            working = working && false;
        }
    }
    private async Task TestRdsS3ClientAsync(RdsS3ResourceTypeConfiguration rdsS3Configuration)
    {
        var (ecsClientMessage, ecsBucketCount) = await S3ClientCheck(rdsS3Configuration.Endpoint, rdsS3Configuration.AccessKey, rdsS3Configuration.SecretKey);
        if (string.IsNullOrWhiteSpace(ecsClientMessage))
        {
            Console.WriteLine(string.Format(format, "S3 Client Check", "OK", $"Found {ecsBucketCount} entries"));
            working = working && true;
        }
        else
        {
            Console.WriteLine(string.Format(format, "S3 Client Check", "FAILED", ecsClientMessage));
            working = working && false;
        }
    }
    public async Task TestBucketsRdsS3Async(string specificType)
    {
        try
        {
            const string _type = "rdss3";
            var rdsS3EcsManager = CreateRdsS3Manager(_type, specificType);
            var userEcsManager = CreateUserManager(_type, specificType);

            var resourceType = ResourceTypeFactory.Instance.GetResourceType(_type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = BucketName });
            await resourceType.CreateResource(BucketName, Quota);
            _logger.Info("Bucket created {BucketName} with {Quota} GB quota", BucketName, Quota);
            Console.WriteLine(string.Format(format, "Bucket Creation", "OK", $"Bucket creation in namespace successful"));

            var randomFileName = $"{_testPrefix}DELETE-ME-{Guid.NewGuid()}.txt";

            var memoryStream = new MemoryStream();
            byte[] testData = { (byte)'C', (byte)'o', (byte)'S', (byte)'I', (byte)'n', (byte)'e' };

            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            await resourceType.StoreEntry(BucketName, randomFileName, memoryStream);
            _logger.Info("Stored file {randomFileName}", randomFileName);

            var entry = await resourceType.GetEntry(BucketName, randomFileName);
            var entries = await resourceType.ListEntries(BucketName, "");
            Console.WriteLine(string.Format(format, "File Storing", "OK", $"Stored files in bucket: {entries.Count}"));

            await resourceType.DeleteEntry(BucketName, entry!.Key);
            Console.WriteLine(string.Format(format, "File Deletion", "OK", $"Files in bucket deletion successful"));

            var readUser = (resourceType as RdsS3ResourceType)?.RdsS3ResourceTypeConfiguration.AccessKeyRead;
            var writeUser = (resourceType as RdsS3ResourceType)?.RdsS3ResourceTypeConfiguration.AccessKeyWrite;
            _logger.Info("Users read {readUser} and write {writeUser}", readUser, writeUser);

            await userEcsManager.DeleteObjectUser(readUser);
            Console.WriteLine(string.Format(format, "Read-User Deletion", "OK", $"Deletion of read user successful"));
            await userEcsManager.DeleteObjectUser(writeUser);
            _logger.Info("Users successfully deleted read {readUser} and write {writeUser}", readUser, writeUser);
            Console.WriteLine(string.Format(format, "Write-User Deletion", "OK", $"Deletion of write user successful"));
            await rdsS3EcsManager.DeleteBucket(BucketName);
            _logger.Info("Bucket successfully deleted {BucketName}", BucketName);
            Console.WriteLine(string.Format(format, "Bucket Deletion", "OK", $"Deletion of bucket successful"));
            working = working && true;
        }
        catch (Exception e)
        {
            Console.WriteLine(string.Format(format, "Bucket Testing", "FAILED", e.Message));
            working = working && false;
            _logger.Error("RDS S3 Testing error on {BucketName} with {Quota} GB quota", BucketName, Quota);
            _logger.Warn("Don't forget to delete the READ and WRITE users!");
        }
    }


    private async Task TestRdsWormEcsAuthAsync(RdsS3WormResourceTypeConfiguration rdsS3Configuration)
    {
        var ecsObjectUserAuthMessage = await EcsAuthentication(rdsS3Configuration.RdsS3EcsManagerConfiguration);
        if (string.IsNullOrWhiteSpace(ecsObjectUserAuthMessage))
        {
            Console.WriteLine(string.Format(format, "Object-User Auth Check", "OK", $"ECS login & logout for object user successful"));
            working = working && true;
        }
        else
        {
            Console.WriteLine(string.Format(format, "Object-User Auth Check", "FAILED", ecsObjectUserAuthMessage));
            working = working && false;
        }

        var ecsUserAuthMessage = await EcsAuthentication(rdsS3Configuration.UserEcsManagerConfiguration);
        if (string.IsNullOrWhiteSpace(ecsUserAuthMessage))
        {
            Console.WriteLine(string.Format(format, "User Auth Check", "OK", $"ECS login & logout for user successful"));
            working = working && true;
        }
        else
        {
            Console.WriteLine(string.Format(format, "User Auth Check", "FAILED", ecsUserAuthMessage));
            working = working && false;
        }
    }
    private async Task TestRdsWormClientAsync(RdsS3WormResourceTypeConfiguration rdsS3Configuration)
    {
        var (ecsClientMessage, ecsBucketCount) = await S3ClientCheck(rdsS3Configuration.Endpoint, rdsS3Configuration.AccessKey, rdsS3Configuration.SecretKey);
        if (string.IsNullOrWhiteSpace(ecsClientMessage))
        {
            Console.WriteLine(string.Format(format, "WORM Client Check", "OK", $"Found {ecsBucketCount} entries"));
            working = working && true;
        }
        else
        {
            Console.WriteLine(string.Format(format, "WORM Client Check", "FAILED", ecsClientMessage));
            working = working && false;
        }
    }


    private EcsManager CreateRdsWebManager(string type, string specificType)
    {
        var resourceType = ResourceTypeFactory.Instance.GetResourceType(type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = BucketName }) as RdsResourceType;

        return new EcsManager
        {
            EcsManagerConfiguration = resourceType?.RdsResourceTypeConfiguration.EcsManagerConfiguration
        };
    }
    private EcsManager CreateRdsS3Manager(string type, string specificType)
    {
        var resourceType = ResourceTypeFactory.Instance.GetResourceType(type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = BucketName }) as RdsS3ResourceType;

        return new EcsManager
        {
            EcsManagerConfiguration = resourceType?.RdsS3ResourceTypeConfiguration.RdsS3EcsManagerConfiguration
        };
    }
    private EcsManager CreateUserManager(string type, string specificType)
    {
        var resourceType = ResourceTypeFactory.Instance.GetResourceType(type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = BucketName }) as RdsS3ResourceType;

        return new EcsManager
        {
            EcsManagerConfiguration = resourceType?.RdsS3ResourceTypeConfiguration.UserEcsManagerConfiguration
        };
    }


    private static async Task<(string, int)> S3ClientCheck(string? endpoint, string? accessKey, string? secretKey)
    {
        try
        {
            var amazonS3Config = new AmazonS3Config
            {
                ServiceURL = endpoint,
                ForcePathStyle = true
            };
            using var _s3client = new AmazonS3Client(accessKey, secretKey, amazonS3Config);
            var output = await _s3client.ListBucketsAsync();
            return (string.Empty, output.Buckets.Count);
        }
        catch (Exception e)
        {
            return (e.Message, 0);
        }
    }
    private static async Task<string> EcsAuthentication(EcsManagerConfiguration? EcsManagerConfiguration, string loginType = " ")
    {
        try
        {
            using var client = new CoscineECSManagementClient(EcsManagerConfiguration!.NamespaceAdminName, EcsManagerConfiguration!.NamespaceAdminPassword, EcsManagerConfiguration!.ManagerApiEndpoint);
            await client.Authenticate();
            await client.LogOut();
            return string.Empty;

        }
        catch (Exception e)
        {
            return e.Message;
        }
    }
}
