﻿using Newtonsoft.Json.Converters;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace IPConnectivityChecker.Enums;

[TypeConverter(typeof(StringEnumConverter))]
public enum ResourceTypes
{
    /// <summary>
    /// Test RDS-Web (Research Data Storage) connection
    /// </summary>
    [EnumMember(Value = "rds")]
    RDS_Web,
    /// <summary>
    /// Test RDS-S3 (Simple Storage Service) connection
    /// </summary>
    [EnumMember(Value = "rdss3")]
    RDS_S3,
    /// <summary>
    /// Test RDS-WORM (Write Once, Read Many) connection
    /// </summary>
    [EnumMember(Value = "rdss3worm")]
    RDS_WORM,
}
