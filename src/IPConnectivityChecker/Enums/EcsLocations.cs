﻿using Newtonsoft.Json.Converters;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace IPConnectivityChecker.Enums;

[TypeConverter(typeof(StringEnumConverter))]
public enum EcsLocations
{
    /// <summary>
    /// Test ECS of RWTH Aachen University
    /// </summary>
    [EnumMember(Value = "rwth")]
    RWTH,
    /// <summary>
    /// Test ECS of North Rhine-Westphalia
    /// </summary>
    [EnumMember(Value = "nrw")]
    NRW,
    /// <summary>
    /// Test ECS of TU Dortmund
    /// </summary>
    [EnumMember(Value = "tudo")]
    TUDO,
    /// <summary>
    /// Test ECS of University of Duisburg-Essen
    /// </summary>
    [EnumMember(Value = "ude")]
    UDE
}
