﻿using CommandLine;
using IPConnectivityChecker.Implementations;
using NLog;
using static IPConnectivityChecker.Util.CommandLineOptions;

namespace IPConnectivityChecker;

public static class Program
{
    private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();

    static async Task<int> Main(string[] args)
    {
        try
        {
            bool result = await Parser.Default
                .ParseArguments<BucketManagerOptions>(args)
                .MapResult(async options =>
                {
                    var ecsTester = new EcsTester(options, _logger);
                    var result = await ecsTester.RunAsync();
                    Console.WriteLine();

                    // Is successful, if all are successful
                    return result;
                },
                _ => Task.FromResult(false)
            );

            if (result)
            {
                Console.WriteLine("\nFinished.\n");
                return 0; // Exit Code 0 for Success
            }
            else
            {
                Console.WriteLine("Program execution was interrupted.\n");
                return -1; // Exit Code -1 for Failure
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return -1; // Exit Code -1 for Failure
        }
    }
}