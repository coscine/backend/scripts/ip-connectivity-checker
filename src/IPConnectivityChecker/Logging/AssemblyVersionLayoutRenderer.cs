﻿using NLog;
using NLog.LayoutRenderers;
using System.Reflection;
using System.Text;

namespace IPConnectivityChecker.Logging
{
    [LayoutRenderer("assembly-version")]
    public class AssemblyVersionLayoutRenderer : LayoutRenderer
    {
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            var assembly = Assembly.GetExecutingAssembly().GetName();
            if (assembly is not null && assembly.Version is not null)
            {
                builder.Append(assembly.Version.ToString(3));
            }
            else
            {
                builder.Append(new Version().ToString(3));
            }
        }
    }
}
